package com.lucaongaro.minhash

/**
 * A type class for creating a colection of hashes of an object given some seeds
 */
trait Hashable[T] {
  def hashes( elem: T, seeds: Seq[Int] ): Seq[Hash]
}

object Hashable {
  import scala.util.hashing.{ MurmurHash3 => MM3 }

  implicit object HashableString extends Hashable[String] {
    def hashes( elem: String, seeds: Seq[Int] ): Seq[Hash] =
      seeds.map( MM3.stringHash( elem, _ ) )
  }

  implicit object HashableInt extends Hashable[Int] {
    def hashes( elem: Int, seeds: Seq[Int] ): Seq[Hash] =
      seeds.map { seed => MM3.finalizeHash( MM3.mixLast( seed, elem ), 1 ) }
  }
}
